import { combineReducers } from "redux";
import token from './TokenReducer'
import user from './user'

const IndexReducer = combineReducers({
    token: token,
    user: user
})

export default IndexReducer;