import AsyncStorage from "@react-native-community/async-storage"
import { ToastAndroid } from 'react-native'

module.exports = {
    setData: async (nama, data) => {
        try {
            await AsyncStorage.setItem(`@${nama}`, data)
        } catch (e) {
            ToastAndroid.show('Gagal', ToastAndroid.SHORT)
        }
    },

    getData: async (nama) => {
        try {
            const data = await AsyncStorage.getItem(`@${nama}`)
            if (data !== null) {
                return data
            } else {
                await AsyncStorage.setItem(`@${nama}`, `null`)
                const wow = await AsyncStorage.getItem(`@${nama}`)
                return wow
            }
        } catch (e) {
            ToastAndroid.show('Gagal', ToastAndroid.SHORT)
        }
    }
}