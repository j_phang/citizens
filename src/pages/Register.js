import React, { Component } from 'react'
import { Image, Dimensions, Modal, ToastAndroid } from 'react-native'
import { Text, View, Container, DatePicker, Content, Item, Picker, Label, Form, Input, Icon, Button, Spinner, Header, Left, Right, Body } from 'native-base'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            email: '',
            password: '',
            gender: 'M',
            date: null,
            fullname: '',
            address: '',
            Modal: false
        }
    }

    register = async () => {
        this.setState({
            Modal: true
        })
        const regis = async (objParam) => await Axios.post(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/',
            objParam
        )
        regis({
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            gender: this.state.gender,
            birthday: this.state.date,
            fullname: this.state.fullname,
            address: this.state.address
        })
            .then(res => {
                switch (true) {
                    case this.state.password !== '':

                }
                this.setState({
                    Modal: false
                })
                this.props.navigation.navigate('Verification', {email: this.state.email})
            })
            .catch(err => {
                this.setState({
                    Modal: false
                })
            })
    }

    regex = () => {
        let email = /^(\w.*\@\w.*\.\w.*)$/
        let password = /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()-+=;"',.<>])(?=.*[\d]).*$/
        if (email.test(this.state.email) && password.test(this.state.password) === true) {
            this.register()
        } else {
            ToastAndroid.show('Email atau Password belum memenuhi persyaratan!', ToastAndroid.SHORT)
        }
    }

    render() {
        return (
            <Container
                style={{
                    flex: 1,
                    backgroundColor: '#4D4D4D'
                }}
            >
                 <Header style={{
                    display: 'flex',
                    backgroundColor: '#4D4D4D'
                    }}>
                    <Left style={{flex: 1}}>
                        <Text style={{color: '#DFDFDF'}} onPress={() => this.props.navigation.goBack()}>Cancel</Text>
                    </Left>
                    <Body style={{
                            flex: 1
                        }}
                    >
                        <Image
                            source={require('../Logos/Logo.png')}
                            style={{
                                width: '100%',
                                height: '100%'
                            }}
                        />
                    </Body>
                    <Right style={{flex: 1}}>
                        <Text style={{color: '#4D4D4D'}}>Hho</Text>
                    </Right>
                </Header>
                <Content style={{ marginVertical: 10}}>
                    <Text style={{color: '#DFDFDF', marginVertical: 10, marginHorizontal: 10}}>Welcome to Citizen News</Text>
                    <Text style={{fontFamily: 'Playfair', marginHorizontal: 10, fontSize: 30, color: '#DFDFDF'}}>Complete Your</Text>
                    <Text style={{fontFamily: 'Playfair', marginHorizontal: 10, fontSize: 30, color: '#DFDFDF'}}>Personal Information</Text>
                    <Text style={{color: '#DFDFDF', marginHorizontal: 10, marginTop: 20, marginBottom: 10}}>Create a Citizens News account to unlock all the benefit:</Text>
                    <Item style={{borderColor: 'transparent'}}>
                        <Icon
                            type='Ionicons'
                            name='checkmark-circle'
                            style={{
                                color: '#DFDFDF',
                                fontSize: 17,
                                marginHorizontal: 10
                            }}
                        />
                        <Text style={{color:'#DFDFDF'}}>
                            Upload your own news.
                        </Text>
                    </Item>
                    <Item style={{borderColor: 'transparent'}}>
                        <Icon
                            type='Ionicons'
                            name='checkmark-circle'
                            style={{
                                color: '#DFDFDF',
                                fontSize: 17,
                                marginHorizontal: 10
                            }}
                        />
                        <Text style={{color:'#DFDFDF'}}>
                            Subscribe to another contributor.
                        </Text>
                    </Item>
                    <Form style={{
                        width: Dimensions.get('window').width * 4.5 / 5,
                        marginTop: 30,
                        alignSelf: 'center'
                    }}
                    >
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF' }}>Username (this will be your login ID)</Label>
                            <Input
                                onChangeText={(text) => this.setState({ username: text })}
                                autoCapitalize='none'
                                style={{
                                    color: '#DFDFDF'
                                }}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF' }}>Fullname</Label>
                            <Input
                                onChangeText={(text) => this.setState({ fullname: text })}
                                style={{
                                    color: '#DFDFDF'
                                }}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF' }}>Email</Label>
                            <Input
                                onChangeText={(text) => this.setState({ email: text })}
                                autoCapitalize='none'
                                style={{
                                    color: '#DFDFDF'
                                }}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF' }}>Password</Label>
                            <Input
                                secureTextEntry={true}
                                onChangeText={(text) => this.setState({ password: text })}
                                style={{
                                    color: '#DFDFDF',
                                    fontSize: 15
                                }}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF' }}>Address</Label>
                            <Input
                                onChangeText={(text) => this.setState({ address: text })}
                                style={{
                                    color: 'white'
                                }}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF'}}>Gender</Label>
                                <Item picker>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ color: "#DFDFDF" }}
                                        selectedValue={this.state.gender}
                                        onValueChange={(value) => this.setState({ gender: value })}
                                    >
                                        <Picker.Item label="Male" value='M' />
                                        <Picker.Item label="Female" value='F' />
                                    </Picker>
                                </Item>
                        </Item>
                        <Item stackedLabel>
                            <Label style={{ color: '#DFDFDF' }}>Birthday</Label>
                                <DatePicker
                                    defaultDate={new Date(1990, 10, 10)}
                                    maximumDate={new Date(2019, 10, 23)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Select date"
                                    textStyle={{ color: "#DFDFDF" }}
                                    placeHolderTextStyle={{ color: "#dfdfdf" }}
                                    disabled={false}
                                    onDateChange={(data) => this.setState({ date: data })}
                                />
                        </Item>
                        <Button
                            onPress={
                                () => {
                                    if (this.state.username === '' || this.state.email === '' || this.state.password === '' || this.state.date === null || this.state.fullname === '' || this.state.address === '') {
                                        ToastAndroid.show('Please Fill the Form!', ToastAndroid.SHORT)
                                    } else {
                                        this.regex()
                                    }
                                }
                            }
                            style={{
                                    marginVertical: 20,
                                    justifyContent: 'center',
                                    alignSelf: 'center',
                                    backgroundColor: '#006AB9',
                                    width: Dimensions.get('window').width * 3 / 5,
                                    height: 35,
                                    borderRadius: 10
                                }}
                        >
                            <Text style={{ alignSelf: 'center', color: 'white' }}>
                                Register
                            </Text>
                        </Button>
                    </Form>
                    <Button transparent
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center'
                        }}
                        onPress={() => this.props.navigation.navigate('Login')}
                    >
                        <Text style={{ color: '#DFDFDF' }}>
                            Already a Citizens? Login
                        </Text>
                    </Button>
                </Content>
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={this.state.Modal}
                    onRequestClose={
                        () => Alert.alert(`Login Berhasil`)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </Container>
        )
    }
}

export default withNavigation(Register)