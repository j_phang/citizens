import React, { Component } from 'react'
import { Text, Image } from 'react-native'
import { Container, Header, Body, Left, Right, Item, Content, Button, Icon } from 'native-base'
import { withNavigation } from 'react-navigation'
const photos = require('../Logos/Logo.png')

class Verification extends Component {
    render() {
        return (
            <Container style={{backgroundColor: '#4D4D4D'}}>
                <Header
                style={{
                    display: 'flex',
                    backgroundColor: '#4D4D4D'
                }}
            >
                <Left
                    style={{
                        flex: 1
                    }}
                >
                    <Text style={{color: '#4D4D4D'}}>
                        Close
                    </Text>
                </Left>
                <Body
                    style={{
                        flex: 1
                    }}
                >
                    <Image
                        source={photos}
                        style={{
                            width: '100%',
                            height: '100%'
                        }}
                    />
                </Body>
                <Right />
            </Header>
            <Content style={{marginHorizontal: 20, marginVertical: 20}}>
            <Text style={{ fontFamily: 'Playfair', fontSize: 34, color: '#DFDFDF', marginVertical: 20}}>Reset Password Confirmed</Text>
                    <Item style={{borderColor: 'transparent'}}>
                        <Text style={{color:'#DFDFDF', fontSize: 15, marginVertical: 20}}>
                            {this.props.navigation.state.params.email}
                        </Text>
                        <Icon
                            type='Ionicons'
                            name='checkmark-circle'
                            style={{
                                color: '#DFDFDF',
                                fontSize: 15,
                                marginHorizontal: 5
                            }}
                        />
                    </Item>
                    <Text style={{ fontSize: 15, color: "#DFDFDF", marginVertical: 5}}>
                        A reset password instruction has been seen to your e-mail.
                    </Text>
                    <Text style={{ fontSize: 15, color: "#DFDFDF", marginVertical: 5 }}>
                    Please follow the instruction at that e-mail to reset your  password.
                    </Text>
                    <Button
                        onPress={() => this.props.navigation.navigate('Login')}
                        style={{
                            marginTop: 200,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            backgroundColor: '#3EA6FE',
                            width: 300,
                            height: 40,
                            borderRadius: 10
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#FFF',
                                fontWeight: 'bold'
                            }}
                        >
                            Close
                        </Text>
                    </Button>
            </Content>
            </Container>
        )
    }
}

export default withNavigation(Verification)