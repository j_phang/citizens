import React, { Component } from "react";
import {
  Text,
  Image,
  Modal,
  ToastAndroid
} from "react-native";
import { withNavigation } from "react-navigation";
import { token } from "../redux/action/TokenAction";
import { connect } from "react-redux";
import {
  Container,
  Content,
  Spinner,
  Item,
  Right,
  Left,
  Thumbnail,
  Body,
  Button,
  Card,
  CardItem,
  Textarea,
  View
} from "native-base";
import * as Storage from '../functions/async'
import Header_news from "../components/Header_news";
import Axios from "axios"
import Video from 'react-native-video'

const mapStateToProps = state => ({
  token: state.token,
  user: state.user
});

const mapDispatchToProps = dispatch => {
  return {
    FetchToken: data => dispatch(token(data))
  };
};

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        category: [],
        date: "",
        description: "",
        listComment: [],
        media: {
          secure_url: "some string"
        },
        region: [],
        status: "",
        title: "",
        user: {
          _id: "",
          username: ""
        }
      },
      loading: true,
      comment: "",
      subscribe: false
    };
  }

  details = async () => {
    const api = async () =>
      await Axios.get(
        `https://app-citizenjournalism.herokuapp.com/api/v1/news/id/${this.props.navigation.getParam("id", null)}`
      );

    api()
      .then(res => {
        this.setState({ data: res.data.result });
        if (this.props.token.token !== 'null') {
          this.subbed()
        } else {
          this.setState({
            loading: false
          })
        }
      })
      .catch(err => {
        console.log(err)
      })
  };

  comment = async () => {
    post = async objParam =>
      await Axios.post(
        "https://app-citizenjournalism.herokuapp.com/api/v1/comment/add",
        objParam,
        {
          headers: {
            Authorization: `Bearer ${this.props.token.token}`
          }
        }
      );

    if (this.state.comment === '') {
      ToastAndroid.show('Please Fill The Comment First!', ToastAndroid.SHORT)
    } else {
      post({
        news_id: this.props.navigation.getParam("id", null),
        comment: this.state.comment
      }).then(res => {
        ToastAndroid.show("Comment Added", ToastAndroid.SHORT);
      });
    }
  };

  subs = () => {
    const api = async () => await Axios.post(
      `https://app-citizenjournalism.herokuapp.com/api/v1/subs/${this.state.data.user._id}`,
      null,
      {
        headers: {
          Authorization: `Bearer ${this.props.token.token}`
        }
      }
    )

    api()
      .then(res => {
        ToastAndroid.show('Berhasil Subscreb!', ToastAndroid.SHORT)
      })
      .catch(err => {
        ToastAndroid.show('Failed, Try Again!', ToastAndroid.SHORT)
      })
  }

  subbed = () => {
    const api = async () => await Axios.get(
      `https://app-citizenjournalism.herokuapp.com/api/v1/subs/${this.state.data.user._id}`,
      {
        headers: {
          Authorization: `Bearer ${this.props.token.token}`
        }
      }
    )

    api()
      .then(res => {
        this.setState({
          loading: false,
          subscribe: res.data.result
        })
      })
      .catch(err => {
        this.setState({
          loading: false
        })
      })
  }

  unsub = () => {
    const api = async () => await Axios.delete(
      `https://app-citizenjournalism.herokuapp.com/api/v1/subs/${this.state.data.user._id}`,
      {
        headers: {
          Authorization: `Bearer ${this.props.token.token}`
        }
      }
    )

    api()
      .then(res => {
        ToastAndroid.show('Unsubbed!!', ToastAndroid.SHORT)
      })
  }

  fetchToken = async () => {
    const tokens = await Storage.getData('token')
    this.props.FetchToken(tokens)
  }

  detail;

  componentDidMount() {
    this.detail = setInterval(this.details, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.detail);
  }

  render() {
    const loops = this.state.data.listComment.map(value => {
      return (
        <Card transparent key={value._id}>
          <CardItem style={{ backgroundColor: "#DFDDDD" }}>
            <Left>
              <Thumbnail source={{ uri: value.user_id.image.secure_url }} />
            </Left>
            <Body style={{ flexDirection: "column", justifyContent: "center" }}>
              <Text style={{ fontWeight: "bold" }}>
                {value.user_id.fullname}
              </Text>
              <Text note style={{ width: 230 }}>{value.comment}</Text>
            </Body>
            <Right />
          </CardItem>
        </Card>
      );
    });
    const media = this.state.data.media.secure_url.split('.')
    if (media[3] == 'mp4') {
      return (
        <Container>
          <Header_news />
          <Content>
            <Text
              style={{
                marginTop: 5,
                marginHorizontal: 20,
                fontSize: 14,
                fontWeight: "bold",
                color: '#0079D4'
              }}
            >
              {this.state.data.category[0]}
            </Text>
            <Text
              style={{
                fontSize: 18,
                alignSelf: "center",
                marginVertical: 5,
                marginHorizontal: 20,
                fontWeight: 'bold'
              }}
            >
              {this.state.data.title}
            </Text>
            <Video
              source={{ uri: this.state.data.media.secure_url }}
              resizeMode='cover'
              style={{ width: 370, height: 220, margin: 10, alignSelf: "center" }}
              controls={true}
            />
            <Text style={{ marginHorizontal: 20 }}
              onPress={() =>
                this.props.navigation.navigate("ProfileDetail", {
                  id: this.state.data.user._id
                })
              }
            >
              By {this.state.data.user.username}
            </Text>
            <Item>
              <Text style={{ marginVertical: 5, marginHorizontal: 20, textAlign: 'justify', lineHeight: 20 }}>
                {this.state.data.description}
              </Text>
            </Item>
            <Item style={{ flexDirection: 'column', alignItems: 'flex-start', flex: 1 }}>
              <Text style={{ marginHorizontal: 20, fontWeight: 'bold' }}>
                Subscribe Contributor
              </Text>
              <Text note style={{ marginHorizontal: 20 }}>
                Get notified when something is published by a contributor you subscribe!
              </Text>
              <Item style={{ flexDirection: 'row' }}>
                <Left>
                  <Text style={{ marginLeft: 20, marginVertical: 5 }}>
                    {this.state.data.user.username}
                  </Text>
                </Left>
                <Right>
                  <Button style={{ width: 100, height: 30, justifyContent: 'center', marginRight: 20, backgroundColor: this.state.subscribe ? 'black' : 'white' }}
                    onPress={() => {
                      if (this.props.token.token !== 'null') {
                        if (this.state.subscribe) {
                          this.unsub()
                        } else {
                          this.subs()
                        }
                      } else {
                        this.props.navigation.navigate('Login')
                      }
                    }}
                  >
                    <Text style={{ color: this.state.subscribe ? 'white' : 'black' }}>Subscribe</Text>
                  </Button>
                </Right>
              </Item>
            </Item>
            <Content style={{ marginHorizontal: 20 }}>
              <Text style={{ fontSize: 16 }}> Comments </Text>
              <Item style={{ borderColor: "black", width: "100%" }}>
                <Body style={{ flexDirection: "row" }}>
                  <Textarea
                    rowSpan={1.5}
                    width="80%"
                    placeholder="Add Comment"
                    onChangeText={text => this.setState({ comment: text })}
                    value={this.state.comment}
                  />
                  <Button
                    transparent
                    onPress={() => {
                      if (this.props.token.token !== "null") {
                        this.comment();
                        this.setState({ comment: '' });
                      } else {
                        this.props.navigation.navigate('Login')
                      }
                    }}
                  >
                    <Text style={{ marginHorizontal: 20 }}>Send</Text>
                  </Button>
                </Body>
              </Item>
              {loops}
            </Content>
          </Content>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.loading}
            onRequestClose={() =>
              ToastAndroid.show(
                "Wait a minute!",
                ToastAndroid.SHORT
              )
            }
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,1)",
                display: "flex",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Spinner />
            </View>
          </Modal>
        </Container>
      );
    } else {
      return (
        <Container>
          <Header_news />
          <Content>
            <Text
              style={{
                marginTop: 10,
                marginHorizontal: 20,
                fontSize: 14,
                fontWeight: 'bold',
                color: '#0079D4'
              }}
            >
              {this.state.data.category[0]}
            </Text>
            <Text
              style={{
                fontSize: 24,
                alignSelf: "center",
                marginVertical: 10,
                marginHorizontal: 20,
                fontFamily: 'Playfair'
              }}
            >
              {this.state.data.title}
            </Text>
            <Image
              source={{ uri: this.state.data.media.secure_url }}
              style={{ width: 370, height: 220, margin: 10, alignSelf: "center" }}
            />
            <Text
              style={{ marginHorizontal: 20, marginVertical: 10, fontFamily: 'Playfair-Italic', fontSize: 16 }}
              onPress={() =>
                this.props.navigation.navigate("ProfileDetail", {
                  id: this.state.data.user._id
                })
              }
            >
              By: {this.state.data.user.username}
            </Text>
            <Item>
              <Text style={{ marginVertical: 5, marginHorizontal: 20, textAlign: 'justify', lineHeight: 22, fontFamily: 'Playfair' }}>
                {this.state.data.description}
              </Text>
            </Item>
            <Item style={{ flexDirection: 'column', alignItems: 'flex-start', flex: 1, marginVertical: 0 }}>
              <Text style={{ marginHorizontal: 20, fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                Subscribe Contributor
              </Text>
              <Text note style={{ marginHorizontal: 20 }}>
                Get notified when something is published by a contributor you subscribe!
              </Text>

              <Item style={{ flexDirection: 'row' }}>
                <Left>
                  <Text style={{ marginLeft: 20, marginVertical: 5 }}>
                    {this.state.data.user.username}
                  </Text>
                </Left>
                <Right style={{ marginVertical: 10}}>
                  <Button style={{ width: 100, height: 30, justifyContent: 'center', marginRight: 20, backgroundColor: this.state.subscribe ? 'black' : 'white' }}
                    onPress={() => {
                      if (this.props.token.token !== 'null') {
                        if (this.state.subscribe) {
                          this.unsub()
                        } else {
                          this.subs()
                        }
                      } else {
                        this.props.navigation.navigate('Login')
                      }
                    }}
                  >
                    <Text style={{ color: this.state.subscribe ? 'white' : 'black' }}>Subscribe</Text>
                  </Button>
                </Right>
              </Item>
            </Item>
            <Content style={{ marginHorizontal: 20, marginVertical: 0 }}>
              <Text style={{ fontSize: 16, fontWeight: 'bold', marginVertical: 20 }}> Comments </Text>
              <Item style={{ borderColor: "black", width: "100%" }}>
                <Body style={{ flexDirection: "row" }}>
                  <Textarea
                    rowSpan={1.5}
                    width="80%"
                    placeholder="Add Comment"
                    onChangeText={text => this.setState({ comment: text })}
                    value={this.state.comment}
                  />
                  <Button
                    transparent
                    onPress={() => {
                      if (this.props.token.token !== "null") {
                        this.comment();
                        this.setState({ comment: null });
                      } else {
                        this.props.navigation.navigate('Login')
                      }
                    }}
                  >
                    <Text style={{ marginHorizontal: 20 }}>Send</Text>
                  </Button>
                </Body>
              </Item>
              {loops}
            </Content>
          </Content>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.loading}
            onRequestClose={() =>
              ToastAndroid.show(
                "Wait a minute!",
                ToastAndroid.SHORT
              )
            }
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,1)",
                display: "flex",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Spinner />
            </View>
          </Modal>
        </Container>
      );
    }
  }
}

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(News)
);
