import React, { Component } from 'react'
import { ToastAndroid, Image, Dimensions, Modal } from 'react-native'
import { Text, Container, View, Item, Content, Label, Input, Button, Spinner, Header, Left, Body, Right } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'
import { withNavigation } from 'react-navigation'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import Axios from 'axios'
import * as Storage from '../functions/async'
import firebase from 'react-native-firebase'

const mapStateToProps = state => ({
    token: state.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data))
    };
};

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            Modal: false
        }
    }

    login = async () => {
        firebase.messaging().getToken()
            .then(res => {
                console.log(res)
                log({
                    username: this.state.username,
                    password: this.state.password,
                    registrationToken: res
                })
                    .then(res => {
                        ToastAndroid.show('Login Success!', ToastAndroid.SHORT)
                        this.setState({
                            Modal: false
                        })
                        this.props.FetchToken(res.data.result.token)
                        Storage.setData('token', res.data.result.token)
                        this.props.navigation.navigate('Home')
                        console.log(res)
                    })
                    .catch(err => {
                        this.setState({
                            password: null,
                            Modal: false
                        })
                        ToastAndroid.show('Account not found!', ToastAndroid.SHORT)
                    })
            })

        const log = async (objParam) => await Axios.post(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/login',
            objParam
        )
    }

    render() {
        return (
            <Container
                style={{
                    backgroundColor: '#4D4D4D'
                }}
            >
                <Header style={{
                    display: 'flex',
                    backgroundColor: '#4D4D4D'
                }}>
                    <Left style={{ flex: 1 }}>
                        <Text style={{ color: '#4D4D4D' }}>hoho</Text>
                    </Left>
                    <Body style={{
                        flex: 1
                    }}
                    >
                        <Image
                            source={require('../Logos/Logo.png')}
                            style={{
                                width: '100%',
                                height: '100%'
                            }}
                        />
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Text style={{ color: '#4D4D4D' }}>Hho</Text>
                    </Right>
                </Header>
                <Text
                    style={{
                        alignSelf: 'center',
                        fontSize: 32,
                        fontFamily: 'Playfair',
                        color: '#DFDFDF',
                        marginVertical: 15
                    }}
                >
                    Sign In
                    </Text>
                <Content style={{ marginVertical: 20 }}>
                    <Item
                        stackedLabel
                        style={{
                            alignSelf: 'center',
                            width: Dimensions.get('window').width * 4 / 5,
                            borderColor: '#DFDFDF'
                        }}
                    >
                        <Label
                            style={{
                                color: '#DFDFDF'
                            }}
                        >
                            Username
                        </Label>
                        <Input
                            style={{
                                color: '#DFDFDF'
                            }}
                            autoCapitalize='none'
                            onChangeText={(text) => {
                                this.setState({
                                    username: text
                                })
                            }}
                        />
                    </Item>
                    <Item
                        stackedLabel
                        style={{
                            alignSelf: 'center',
                            width: Dimensions.get('window').width * 4 / 5,
                            borderColor: '#DFDFDF',
                            marginBottom: 20
                        }}
                    >
                        <Label
                            style={{
                                color: '#DFDFDF'
                            }}
                        >
                            Password
                        </Label>
                        <Input
                            secureTextEntry={true}
                            style={{
                                color: '#DFDFDF'
                            }}
                            value={this.state.password}
                            onChangeText={(text) => {
                                this.setState({
                                    password: text
                                })
                            }}
                        />
                    </Item>
                    <Button
                        onPress={
                            () => {
                                if (this.state.username === '' && this.state.password === '') {
                                    ToastAndroid.show('Username dan Password Kosong!', ToastAndroid.SHORT)
                                } else if (this.state.username === '' || this.state.password === '') {
                                    ToastAndroid.show('Username atau Password Kosong!', ToastAndroid.SHORT)
                                } else {
                                    this.setState({
                                        Modal: true
                                    })
                                    this.login()
                                }
                            }
                        }
                        style={{
                            marginVertical: 10,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            backgroundColor: '#006AB9',
                            width: Dimensions.get('window').width * 3 / 5,
                            height: 35,
                            borderRadius: 10
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center'
                            }}
                        >
                            Sign In
                        </Text>
                    </Button>
                    <Button
                        transparent
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center'
                        }}
                        onPress={() => this.props.navigation.navigate('Register')}
                    >
                        <Text
                            style={{
                                color: '#DFDFDF'
                            }}
                        >
                            Don't have any account? Register
                        </Text>
                    </Button>
                    <Button
                        transparent
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center'
                        }}
                        onPress={
                            () => this.props.navigation.navigate('ForgotPassword')
                        }
                    >
                        <Text
                            style={{
                                color: '#DFDFDF'
                            }}
                        >
                            forgot my password
                        </Text>
                    </Button>
                </Content>
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={this.state.Modal}
                    onRequestClose={
                        () => Alert.alert(`Success Login`)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Login))