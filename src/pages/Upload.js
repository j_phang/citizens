import React, { Component } from 'react'
import { Image, Dimensions, ToastAndroid, Modal } from 'react-native'
import { withNavigation } from 'react-navigation'
import {
    Container,
    Text,
    Header,
    Left,
    Body,
    Right,
    Icon,
    Content,
    Item,
    Input,
    Picker,
    View,
    Spinner
} from 'native-base'
import Axios from 'axios'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import { getUser } from '../redux/action/user'
import ImagePicker from 'react-native-image-picker'
import Login from './Login'

const mapStateToProps = state => ({
    token: state.token.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data)),
        GetUser: token => dispatch(getUser(token))
    };
};

class Upload extends Component {

    constructor(props) {
        super(props)
        this.state = {
            active: false,
            title: '',
            desc: '',
            category: 'news',
            media: null,
            Modal: false,
            medloc: ''
        }
    }

    media = () => {
        const options = {
            title: 'Select Profile!',
            mediaType: 'mixed'
        }

        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                ToastAndroid.show(`Canceled!`, ToastAndroid.SHORT)
            } else if (response.error) {
                ToastAndroid.show(`Sorry! Please Choose again!`, ToastAndroid.SHORT)
            } else {
                this.setState({
                    media: response,
                    medloc: response.uri
                })
            }
        })
    }

    upload = async () => {
        let moment = require('moment-timezone')
        console.log(moment('20121004').tz("Asia/Bangkok").format('LLL'))
        const uplud = async () => await Axios.post(
            'https://app-citizenjournalism.herokuapp.com/api/v1/news/create',
            {
                title: this.state.title,
                description: this.state.desc,
                category: this.state.category,
                date: moment('20121004').tz("Asia/Bangkok").format('LLL')
            },
            {
                headers: {
                    'Authorization': `Bearer ${this.props.token}`
                }
            }
        )
        if (this.state.media === null || this.state.title === '' || this.state.desc === '') {
            ToastAndroid.show('Masukkan Data!', ToastAndroid.SHORT)
            this.setState({
                Modal: false
            })
        } else {
            uplud()
                .then(res => {
                    this.add_media(res.data.result._id)
                })
        }
    }

    add_media = async (id) => {
        let photo = new FormData()

        photo.append('image',
            {
                uri: this.state.media.uri,
                name: this.state.media.fileName,
                type: this.state.media.type
            }
        )

        const api = async () => await Axios.post(
            `https://app-citizenjournalism.herokuapp.com/api/v1/news/upload/${id}`,
            photo,
            {
                headers: {
                    Authorization: `Bearer ${this.props.token}`
                }
            }
        )

        if (this.state.media === null) {
            ToastAndroid.show(`Belum Ada Foto!`, ToastAndroid.SHORT)
            this.setState({
                Modal: false
            })
        } else {
            api()
                .then(res => {
                    ToastAndroid.show('Berhasil cuy!', ToastAndroid.SHORT)
                    this.setState({
                        Modal: false,
                        tipathtle: '',
                        depathsc: '',
                        category: 'news',
                        media: null
                    })
                })
        }
    }

    render() {
        if (this.props.token === 'null') {
            return <Login />
        } else {
            if (this.state.media === null) {
                return (
                    <Container>
                        <Header
                            style={{
                                display: 'flex',
                                backgroundColor: '#FFF'
                            }}
                        >
                            <Left
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                }}
                            >
                                <Text style={{ color: '#006AB9', fontSize: 15 }} onPress={() => this.props.navigation.goBack()}>
                                    Cancel
                                </Text>
                            </Left>
                            <Body
                                style={{
                                    flex: 1
                                }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: "bold", alignSelf: 'center' }} onPress={() => this.props.navigation.goBack()}>
                                    Add News
                                </Text>
                            </Body>
                            <Right
                                style={{
                                    flex: 1
                                }}
                            >
                                <Text style={{ color: '#006AB9', fontSize: 15, fontWeight: "bold" }}
                                    onPress={
                                        () => {
                                            this.setState({
                                                Modal: true
                                            })
                                            this.upload()
                                        }
                                    }>
                                    Publish
                                </Text>
                            </Right>
                        </Header>
                        <Content
                            style={{
                                backgroundColor: '#FFF',
                                flex: 1
                            }}
                        >
                            <Item
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    width: Dimensions.get('window').width * 4.5 / 5,
                                    alignSelf: 'center'
                                }}
                            >
                                <Picker
                                    note
                                    placeholder="Select Category"
                                    mode="dropdown"
                                    style={{ width: '100%' }}
                                    selectedValue={this.state.category}
                                    onValueChange={(text) => this.setState({ category: text })}
                                >
                                    <Picker.Item label="News" value="news" />
                                    <Picker.Item label="Education" value="education" />
                                    <Picker.Item label="Lifestyle" value="lifestyle" />
                                    <Picker.Item label="Tech" value="tech" />
                                    <Picker.Item label="Entertainment" value="entertaiment" />
                                    <Picker.Item label="Food" value="food" />
                                    <Picker.Item label="Video" value="video" />
                                </Picker>
                            </Item>
                            <Item style={{
                                flex: 1,
                                marginVertical: 20,
                                width: Dimensions.get('window').width * 4 / 5,
                                backgroundColor: "#DFDFDF",
                                alignSelf: 'center',
                                height: 200,
                                flexDirection: "column"
                            }}
                                onPress={
                                    () => this.media()
                                }
                            >
                                <Icon
                                    type='Ionicons'
                                    name='image'
                                    style={{
                                        color: '#919191',
                                        fontSize: 44,
                                        marginTop: 70
                                    }}
                                />
                                <Text style={{ color: '#919191' }}>
                                    Tap to add media
                                </Text>
                            </Item>
                            <Item
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    marginVertical: 10,
                                    width: Dimensions.get('window').width * 4.5 / 5,
                                    alignSelf: 'center',
                                    borderColor: '#919191'
                                }}
                            >
                                <Input
                                    placeholder="Insert Your Title Here"
                                    style={{
                                        backgroundColor: 'white',
                                        color: 'black',
                                        width: '100%'
                                    }}
                                    value={this.state.title}
                                    onChangeText={
                                        (text) => {
                                            this.setState({
                                                title: text
                                            })
                                        }
                                    }
                                />
                            </Item>
                            <Item
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    width: Dimensions.get('window').width * 4.5 / 5,
                                    alignSelf: 'center'
                                }}
                            >
                                <Input
                                    placeholder="Insert Your Text Here"
                                    style={{
                                        backgroundColor: '#F1F1F1',
                                        color: 'black',
                                        width: '100%',
                                        height: 300,
                                        textAlignVertical: 'top'
                                    }}
                                    value={this.state.desc}
                                    multiline={true}
                                    onChangeText={
                                        (text) => {
                                            this.setState({
                                                desc: text
                                            })
                                        }
                                    }
                                />
                            </Item>
                        </Content>
                        <Modal
                            animationType='fade'
                            transparent={true}
                            visible={this.state.Modal}
                            onRequestClose={
                                () => ToastAndroid.show(`Haha! You can't do that!`, ToastAndroid.SHORT)
                            }
                        >
                            <View
                                style={{
                                    backgroundColor: 'rgba(0,0,0,0.4)',
                                    display: 'flex',
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                            >
                                <Spinner />
                            </View>
                        </Modal>
                    </Container>
                )
            } else {
                return (
                    <Container>
                        <Header
                            style={{
                                display: 'flex',
                                backgroundColor: '#FFF'
                            }}
                        >
                            <Left
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                }}
                            >
                                <Text style={{ color: '#006AB9', fontSize: 15 }} onPress={() => this.props.navigation.goBack()}>
                                    Cancel
                                </Text>
                            </Left>
                            <Body
                                style={{
                                    flex: 1
                                }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: "bold", alignSelf: 'center' }} onPress={() => this.props.navigation.goBack()}>
                                    Add News
                                </Text>
                            </Body>
                            <Right
                                style={{
                                    flex: 1
                                }}
                            >
                                <Text style={{ color: '#006AB9', fontSize: 15, fontWeight: "bold" }}
                                    onPress={
                                        () => {
                                            this.setState({
                                                Modal: true
                                            })
                                            this.upload()
                                        }
                                    }>
                                    Publish
                                </Text>
                            </Right>
                        </Header>
                        <Content
                            style={{
                                backgroundColor: '#FFF',
                                flex: 1
                            }}
                        >
                            <Item
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    width: Dimensions.get('window').width * 4.5 / 5,
                                    alignSelf: 'center'
                                }}
                            >
                                <Picker
                                    note
                                    placeholder="Select Category"
                                    mode="dropdown"
                                    style={{ width: '100%' }}
                                    selectedValue={this.state.category}
                                    onValueChange={(text) => this.setState({ category: text })}
                                >
                                    <Picker.Item label="News" value="news" />
                                    <Picker.Item label="Education" value="education" />
                                    <Picker.Item label="Lifestyle" value="lifestyle" />
                                    <Picker.Item label="Tech" value="tech" />
                                    <Picker.Item label="Entertainment" value="entertaiment" />
                                    <Picker.Item label="Food" value="food" />
                                    <Picker.Item label="Video" value="video" />
                                </Picker>
                            </Item>
                            <Item style={{
                                flex: 1,
                                marginVertical: 20,
                                width: Dimensions.get('window').width * 4 / 5,
                                backgroundColor: "#DFDFDF",
                                alignSelf: 'center',
                                height: 200,
                                flexDirection: "column"
                            }}
                                onPress={
                                    () => this.media()
                                }
                            >
                                <Image
                                    source={{uri:this.state.medloc}}
                                    style={{
                                        width: '100%',
                                        height: '100%'
                                    }}
                                />
                                
                            </Item>
                            <Item
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    marginVertical: 10,
                                    width: Dimensions.get('window').width * 4.5 / 5,
                                    alignSelf: 'center',
                                    borderColor: '#919191'
                                }}
                            >
                                <Input
                                    placeholder="Insert Your Title Here"
                                    style={{
                                        backgroundColor: 'white',
                                        color: 'black',
                                        width: '100%'
                                    }}
                                    value={this.state.title}
                                    onChangeText={
                                        (text) => {
                                            this.setState({
                                                title: text
                                            })
                                        }
                                    }
                                />
                            </Item>
                            <Item
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    width: Dimensions.get('window').width * 4.5 / 5,
                                    alignSelf: 'center'
                                }}
                            >
                                <Input
                                    placeholder="Insert Your Text Here"
                                    style={{
                                        backgroundColor: '#F1F1F1',
                                        color: 'black',
                                        width: '100%',
                                        height: 300,
                                        textAlignVertical: 'top'
                                    }}
                                    value={this.state.desc}
                                    multiline={true}
                                    onChangeText={
                                        (text) => {
                                            this.setState({
                                                desc: text
                                            })
                                        }
                                    }
                                />
                            </Item>
                        </Content>
                        <Modal
                            animationType='fade'
                            transparent={true}
                            visible={this.state.Modal}
                            onRequestClose={
                                () => ToastAndroid.show(`Haha! You can't do that!`, ToastAndroid.SHORT)
                            }
                        >
                            <View
                                style={{
                                    backgroundColor: 'rgba(0,0,0,0.4)',
                                    display: 'flex',
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                            >
                                <Spinner />
                            </View>
                        </Modal>
                    </Container>
                )
            }
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Upload))