import React, { Component } from 'react'
import { Text } from 'react-native'
import { Container, Item, Content, Button } from 'native-base'
import Header_about from '../components/Header_about'
import { withNavigation } from 'react-navigation'
import Video from 'react-native-video'

class AboutUs extends Component {
    render() {
        return (
            <Container style={{flex:1, backgroundColor: '#F4F4F4'}}>
                <Header_about />
                <Content>
                    <Video
                        source={{ uri: 'https://res.cloudinary.com/limkaleb/video/upload/v1571826672/citizen-journalism/t_video6125410279414038663_obfphg.mp4' }}
                        resizeMode='cover'
                        style={{ width: 370, height: 220, marginVertical: 30, alignSelf: "center" }}
                        controls={true}
                    />
                    <Text 
                        style=
                            {{
                                marginVertical: 5,
                                marginHorizontal: 30,
                                fontSize: 15,
                                fontFamily: 'Playfair',
                                lineHeight: 20,
                                textAlign: 'justify'
                            }}
                    >
                        Citizen News  is the platform for you to share your story,
                        to speak up your voice, and to spread the news.
                    </Text>
                    <Text
                        style=
                        {{
                            marginVertical: 10,
                            marginHorizontal: 30,
                            fontSize: 15,
                            fontFamily: 'Playfair',
                            lineHeight: 20,
                            textAlign: 'justify'
                        }}
                    >
                        Proudly developed by: Aldo Lim, Angel Ria Purnamasari,
                        Kaleb Lim, Khairunissa Afifa, and Joe Phang.
                    </Text>
                    <Item style={{ alignSelf: 'center', flexDirection: "column", marginVertical: 20}}>
                        <Text
                            style={{
                                fontFamily: 'Playfair',
                                fontSize: 24,
                                fontWeight: "bold",
                                marginVertical: 10
                            }}
                        >
                            Citizen News
                        </Text>
                        <Text style={{ fontFamily: 'Playfair', fontSize: 20 }}>
                            Celebrating Freedom
                        </Text>
                    </Item>
                    <Button
                        onPress={() => this.props.navigation.navigate('Profile')}
                        style={{
                            marginTop: 20,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            backgroundColor: '#3EA6FE',
                            width: 300,
                            height: 40,
                            borderRadius: 10
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#FFF',
                                fontWeight: 'bold'
                            }}
                        >
                            Close
                        </Text>
                    </Button>
                </Content>
            </Container>
        )
    }
}

export default withNavigation(AboutUs)
