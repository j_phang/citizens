import React, { Component } from 'react'
import { Image, Dimensions } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Container, Header, Left, Text, Body, Right, Icon, View, Content, Card, CardItem, Button, Spinner } from 'native-base'
import Axios from 'axios'
import Video from 'react-native-video'

class CategoryPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [
                {
                    _id: '1',
                    title: 'Contoh ajah',
                    description: 'oaohfahsjkfhajfhaskhajkdfka jhkadaj aljdljkafhjf hffasd hfkas fhskjlf asd jkflsf sdk ff kfsd jkf kfskj sdkfjh sd jkfsjk ff jksdfksd fsakj',
                    media: {
                        secure_url: 'https://res.cloudinary.com/khairunnishafifa/image/upload/v1570095945/news_vtnwhb.png'
                    },
                    date: '12-08-2019'
                }
            ],
            loading: true
        }
    }

    api;

    componentDidMount() {
        this.api = setInterval(this.news, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.api)
    }

    news = () => {
        const api = async () => await Axios.get(
            `https://app-citizenjournalism.herokuapp.com/api/v1/news/findcategory/${this.props.navigation.getParam('id', 'Video')}`
        )
        api()
            .then(res => {
                this.setState({
                    data: res.data.result,
                    loading: false
                })
            })
    }

    render() {
        const loop = this.state.data.map(value => {
            const type = value.media.secure_url.split('.')
            if (type[3] === 'mp4') {
                return (
                    <Card key={value._id}>
                        <Button
                            style={{
                                backgroundColor: '#0079D4',
                                width: '40%',
                                height: 35,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 10,
                                marginVertical: 10,
                                marginHorizontal: 17
                            }}
                        >
                            <Text>{this.props.navigation.getParam('id', 'null')}</Text>
                        </Button>
                        <Video
                            source={{ uri: value.media.secure_url }}
                            resizeMode='cover'
                            style={{ width: 370, height: 220, margin: 10, alignSelf: "center" }}
                            paused={false}
                            muted={true}
                            poster={value.media.secure_url}
                        />
                        <CardItem button onPress={
                            () => {
                                this.props.navigation.navigate('News', {
                                    id: value._id
                                })
                            }
                        }>
                            <Body>
                                <Text
                                    style={{
                                        fontSize: 28,
                                        fontFamily: 'Playfair'
                                    }}
                                >
                                    {value.title}
                                </Text>
                                <Text note style={{ fontSize: 14, fontFamily: 'Playfair', textAlign: 'justify' }}>
                                    {`${value.description.substring(0, 127)}...`}
                                </Text>
                            </Body>
                        </CardItem>
                    </Card>
                )
            } else {
                return (
                    <Card key={value._id}>
                        <Button
                            style={{
                                backgroundColor: '#0079D4',
                                width: '40%',
                                height: 35,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 10,
                                marginVertical: 10,
                                marginHorizontal: 17
                            }}
                        >
                            <Text>{this.props.navigation.getParam('id', 'null')}</Text>
                        </Button>
                        <Image
                            source={{ uri: value.media.secure_url }}
                            style={{
                                width: Dimensions.get('window').width,
                                height: 200,
                                alignSelf: 'center'
                            }}
                        />
                        <CardItem button onPress={
                            () => {
                                this.props.navigation.navigate('News', {
                                    id: value._id
                                })
                            }
                        }>
                            <Body>
                                <Text
                                    style={{
                                        fontSize: 28,
                                        fontFamily: 'Playfair'
                                    }}
                                >
                                    {value.title}
                                </Text>
                                <Text note style={{ fontSize: 14, fontFamily: 'Playfair', textAlign: 'justify' }}>
                                    {`${value.description.substring(0, 127)}...`}
                                </Text>
                            </Body>
                        </CardItem>
                    </Card>
                )
            }
        })
        if (this.state.loading) {
            return (
                <Container>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignContent: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Header
                        style={{
                            display: 'flex',
                            backgroundColor: '#FFF'
                        }}
                    >
                        <Left
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                            }}
                        >
                            <Text
                                style={{
                                    color: '#3EA6FE'
                                }}
                                onPress={
                                    () => this.props.navigation.pop()
                                }
                            >
                                Back
                            </Text>
                        </Left>
                        <Body
                            style={{
                                flex: 1,
                            }}
                        >
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>{this.props.navigation.getParam('id', 'hayoo')}</Text>
                        </Body>
                        <Right
                            style={{
                                flex: 1
                            }}
                        >
                            <Icon
                                type="Ionicons"
                                name="md-exit"
                                style={{
                                    color: 'white'
                                }}
                            />
                        </Right>
                    </Header>
                    <Content

                    >
                        {loop}
                    </Content>
                </Container>
            )
        }
    }
}

export default withNavigation(CategoryPage)