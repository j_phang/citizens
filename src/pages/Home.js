import React, { Component, useEffect } from 'react'
import { Image, ToastAndroid, Modal, Dimensions, FlatList } from 'react-native'
import { Text, Container, Card, CardItem, Body, Content, View, Spinner } from 'native-base'
import { withNavigation } from 'react-navigation'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import * as Storage from '../functions/async'
import Axios from 'axios'
import Header_home from '../components/Header_home'
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';


const mapStateToProps = state => ({
    token: state.token.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data))
    };
};

class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loading: true,
            data: [
                {
                    _id: '09',
                    title: 'empty',
                    description: 'empty',
                    media: {
                        secure_url: 'empty'
                    }
                },
            ]
        }
    }

    componentDidMount = async () => {
        this.ambildata()

        const token = await Storage.getData('@tokens')
        if (token) {
            this.onTokenRefreshListener = firebase
                .messaging()
                .onTokenRefresh(fcmToken => {
                    handlefcmToken(token, fcmToken)
                });

            this.checkPermission();
            const channel = new firebase.notifications.Android.Channel(
                'EventChannel',
                'EventChannel',
                firebase.notifications.Android.Importance.Max,
            ).setDescription('EventChannel');
            firebase.notifications().android.createChannel(channel);
            this.removeNotificationListener = firebase
                .notifications()
                .onNotification((notification: Notification) => {
                    const notif = new firebase.notifications.Notification()
                        .setNotificationId(notification._notificationId)
                        .setTitle(notification._title)
                        .setBody(notification._body)
                        .setData(notification._data)
                        .setSound('default')
                        .android.setChannelId(notification._data.channel_id);
                    firebase.notifications().displayNotification(notif);
                });
            this.removeNotificationOpenedListener = firebase
                .notifications()
                .onNotificationOpened((notificationOpen: NotificationOpen) => {
                    const notification: Notification = notificationOpen.notification;
                    this.props.navigation.push(notification.data.navigate, {
                        eventid: notification.data.eventId,
                    });
                    firebase
                        .notifications()
                        .removeDeliveredNotification(notification._notificationId);
                });
            firebase
                .notifications()
                .getInitialNotification()
                .then((notificationOpen: NotificationOpen) => {
                    if (notificationOpen) {
                        const notification: Notification = notificationOpen.notification;
                        this.props.navigation.push(notification.data.navigate, {
                            eventid: notification.data.eventId,
                        });
                        firebase
                            .notifications()
                            .removeDeliveredNotification(notification._notificationId);
                    }
                });
        }
    }

    async componentWillUnmount() {
        const token = await AsyncStorage.getItem('@tokens')

        if (token) {
            this.onTokenRefreshListener();
            this.removeNotificationListener();
            this.removeNotificationOpenedListener();
        }
    }

    checkPermission = () => {
        firebase
            .messaging()
            .hasPermission()
            .then(enabled => {
                if (enabled) {
                    return null;
                }
                this.requestPermission();
            });
    };

    requestPermission = () => {
        firebase
            .messaging()
            .requestPermission()
            .then(() => null)
            .catch(() => null);
    };

    ambildata = async () => {
        const data = async () => await Axios.get(
            'https://app-citizenjournalism.herokuapp.com/api/v1/news/status/approved'
        )

        const tokens = await Storage.getData('token')
        this.props.FetchToken(tokens)

        data()
            .then(res => {
                this.setState({
                    refreshing: false,
                    loading: false,
                    data: res.data.result
                })
            })
    }

    handleRefresh = () => {
        this.setState({
            refreshing: true,
            data: []
        },
            () => {
                this.ambildata()
            }
        )
    }

    render() {
        if (this.state.loading) {
            return (
                <Container>
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,1)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <View
                        style={{
                            flex: 1, backgroundColor: '#DFDFDF'
                        }}
                    >
                        <Header_home />
                        <FlatList
                            data={this.state.data}
                            renderItem={({ item }) => (
                                <Card
                                    key={item._id}
                                >
                                    <Image
                                        source={{ uri: item.media.secure_url }}
                                        style={{
                                            width: Dimensions.get('window').width,
                                            height: 200,
                                            alignSelf: 'center'
                                        }}
                                    />
                                    <CardItem button onPress={
                                        () => {
                                            this.props.navigation.navigate('News', {
                                                id: item._id
                                            })
                                        }
                                    }>
                                        <Body>
                                            <Text
                                                style={{
                                                    fontSize: 24,
                                                    fontFamily: 'Playfair'
                                                }}
                                            >
                                                {item.title}
                                            </Text>
                                            <Text note
                                                style={{
                                                    marginVertical: 5,
                                                    fontSize: 14,
                                                    fontFamily: 'Playfair'
                                                }}
                                            >
                                                {`${item.description.substring(0, 127)}...`}
                                            </Text>
                                        </Body>
                                    </CardItem>
                                </Card>
                            )}
                            keyExtractor={item => item._id}
                            onRefresh={this.handleRefresh}
                            refreshing={this.state.refreshing}
                        />
                    </View>
                    <Modal
                        animationType='fade'
                        transparent={true}
                        visible={this.state.loading}
                        onRequestClose={
                            () => ToastAndroid.show('Wait a minute!', ToastAndroid.SHORT)
                        }
                    >
                        <View
                            style={{
                                backgroundColor: 'rgba(0,0,0,1)',
                                display: 'flex',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Spinner />
                        </View>
                    </Modal>
                </Container>
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Home))