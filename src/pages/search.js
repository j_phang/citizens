import React, { Component } from 'react'
import { Image, Dimensions, ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Container, Header, Left, Body, Right, Input, Card, CardItem, Content, Button } from 'native-base'
import Axios from 'axios'
import Video from 'react-native-video'

class Search extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }

    search = (query) => {
        const api = async () => Axios.get(
            `https://app-citizenjournalism.herokuapp.com/api/v1/news/find-news?status=Approved&title=${query}`
        )

        api()
            .then(res => {
                this.setState({
                    data: res.data.result
                })
            })
            .catch(err => {
                ToastAndroid.show('File not found!', ToastAndroid.SHORT)
            })
    }

    render() {
        const loops = this.state.data.map(value => {
            const ext = value.media.secure_url.split('.')
            if (ext[3] === 'mp4') {
                return (
                    <Card
                        key={value._id}
                    >
                        <CardItem button onPress={
                            () => {
                                this.props.navigation.navigate('News', {
                                    id: value._id
                                })
                            }
                        }>
                            <Left>
                                <Video
                                    source={{ uri: value.media.secure_url }}
                                    resizeMode='cover'
                                    style={{ width: 120, height: 120, alignSelf: "center" }}
                                    paused={false}
                                    muted={true}
                                    poster={value.media.secure_url}
                                />
                            </Left>
                            <Right
                                style={{ marginLeft: '-40%' }}
                            >
                                <Text>{value.title}</Text>
                            </Right>
                        </CardItem>
                    </Card>
                )
            } else {
                return (
                    <Card
                        key={value._id}
                    >
                        <CardItem button onPress={
                            () => {
                                this.props.navigation.navigate('News', {
                                    id: value._id
                                })
                            }
                        }>
                            <Left>
                                <Image
                                    source={{ uri: value.media.secure_url }}
                                    style={{
                                        width: 120,
                                        height: 120
                                    }}
                                />
                            </Left>
                            <Right
                                style={{ marginLeft: '-40%' }}
                            >
                                <Text>{value.title}</Text>
                            </Right>
                        </CardItem>
                    </Card>
                )
            }
        })
        return (
            <Container style={{ backgroundColor: '#E7e7e7' }}>
                <Header
                    style={{
                        display: 'flex',
                        backgroundColor: '#FFF',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Left
                        style={{
                            flex: 1
                        }}
                    >
                        <Input
                            style={{
                                width: Dimensions.get('window').width,
                                color: 'black'
                            }}
                            placeholder='Search the news by title'
                            onChangeText={
                                (text) => {
                                    this.search(text)
                                }
                            }
                        />
                    </Left>
                    <Body />
                    <Right>
                        <Text
                            style={{ color: '#81B3DA' }}
                            onPress={() => this.props.navigation.pop()}
                        >
                            Cancel
                        </Text>
                    </Right>
                </ Header>
                <Content>
                    {loops}
                </Content>
            </Container>
        )
    }
}

export default withNavigation(Search)