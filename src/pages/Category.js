import React, { Component } from 'react'
import { withNavigation } from 'react-navigation'
import { Container, Text, Header, Left, Body, Right, Icon, Content, List, ListItem } from 'native-base'

class Category extends Component {

    constructor(props) {
        super(props)
        this.state = {
            active: false
        }
    }

    render() {
        return (
            <Container>
                <Header
                    style={{
                        display: 'flex',
                        backgroundColor: '#FFF'
                    }}
                >
                    <Left
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                        }}
                    >
                        <Text
                            style={{
                                color: '#3EA6FE'
                            }}
                            onPress={
                                () => this.props.navigation.pop()
                            }
                        >
                            Cancel
                        </Text>
                    </Left>
                    <Body
                        style={{
                            flex: 1
                        }}
                    >
                        <Text style={{ fontWeight: 'bold', alignSelf: 'center' }}>Category</Text>
                    </Body>
                    <Right
                        style={{
                            flex: 1
                        }}
                    >
                        <Icon
                            type="Ionicons"
                            name="md-exit"
                            style={{
                                color: 'white'
                            }}
                        />
                    </Right>
                </Header>
                <Content
                    style={{
                        backgroundColor: 'white'
                    }}
                >
                    <List>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'News' })}>
                            <Left>
                                <Text>News</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'Education' })}>
                            <Left>
                                <Text>Education</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'Entertainment' })}>
                            <Left>
                                <Text>Entertainment</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'Lifestyle' })}>
                            <Left>
                                <Text>Lifestyle</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'Tech' })}>
                            <Left>
                                <Text>Tech</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'Food' })}>
                            <Left>
                                <Text>Food</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('CategoryPage', { id: 'Video' })}>
                            <Left>
                                <Text>Video</Text>
                            </Left>
                            <Right>
                                <Icon name='arrow-forward' />
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

export default withNavigation(Category)