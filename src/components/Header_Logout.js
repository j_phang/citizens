import React, { Component } from 'react'
import { Image, ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Body, Icon, Right, Header, Left } from 'native-base'

class Header_Logout extends Component {
    render() {
        return (
            <Header
                style={{
                    display: 'flex',
                    backgroundColor: '#FFF'
                }}
            >
                <Left
                    style={{
                        flex: 1
                    }}
                >
                    <Text
                        style={{
                            color: '#FFF'
                        }}
                    > Hho</Text>
                </Left>
                <Body
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Text style={{fontWeight: "bold"}}>
                        Profile
                    </Text>
                </Body>
                <Right
                    style={{
                        flex: 1
                    }}
                >
                    <Icon
                        type="Ionicons"
                        name="md-exit"
                        style={{
                            color: '#FFF'
                        }}
                    />
                </Right>
            </Header>
        )
    }
}
export default withNavigation(Header_Logout)