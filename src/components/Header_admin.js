import React, { Component } from 'react'
import { Image, ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Body, Icon, Right, Header, Left } from 'native-base'
import * as Storage from '../functions/async'

class Header_Logout extends Component {
    render() {
        return (
            <Header
                noShadow
                style={{
                    display: 'flex',
                    backgroundColor: '#FFFFFF'
                }}
            >
                <Left
                    style={{
                        flex: 1
                    }}
                >
                    <Text
                        style={{
                            color: 'white'
                        }}
                    > Hho</Text>
                </Left>
                <Body
                    style={{
                        flex: 1
                    }}
                >
                    <Image
                        source={require('../Logos/Logo_Hitam.png')}
                        style={{
                            width: '100%',
                            height: '100%'
                        }}
                    />
                </Body>
                <Right
                    style={{
                        flex: 1
                    }}
                >
                    <Icon
                        type="Ionicons"
                        name="md-exit"
                        style={{
                            color: 'black'
                        }}
                        onPress={
                            () => {
                                Storage.setData('token', 'null')
                                ToastAndroid.show('Logout', ToastAndroid.SHORT)
                                this.props.navigation.navigate('Home')
                            }
                        }
                    />
                </Right>
            </Header>
        )
    }
}
export default withNavigation(Header_Logout)