import React, { Component } from 'react'
import configureStore from './store'
import { Provider } from 'react-redux'
import { StatusBar } from 'react-native'
import AppContainer from './Stack'
const store = configureStore()

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <StatusBar
          backgroundColor='black'
          barStyle='dark-content'
          hidden={true}
          translucent={true}
        />
        <AppContainer />
      </Provider>
    )
  }
}
